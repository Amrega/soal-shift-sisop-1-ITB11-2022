#!/bin/bash

ord() {
        printf -v ascii "%d" "'$1"
}

echo "welcome to soal 1"
echo "please register yourself"

if [ ! -d ./users ]
then
    mkdir users
    echo "user" >> users/user.txt
    echo "log"  >> users/log.txt
fi

cap=0
num=0
nocap=0
brek=0
same=1

file=$(awk '{FS=":"}{print $1 }' ./users/user.txt)

filearr=(`echo $file | tr ' ' ' '`)

while [ $same == 1 ]
do
        same=0
        echo "enter your username?"
        read username

        for((i=0 ;i<${#filearr} ; i++)){
                if [[ $username == ${filearr[i]} ]]
                then
                        same=1
                        echo "$(date +'%D') $(date +'%T') REGISTER: ERROR user already exist" >> ./users/log.txt
                        echo "REGISTER: ERROR user $username already exist"
                        break
                fi
        }

done

while true
do
        echo "password must contain at least 8 letter and 1 lowercase, 1 uppercase, 1 alphanumeric and cant be same as username"
        echo "enter your password?"
        read -s password

        if [ $username != $password ] && [ ${#password} -ge 8 ]
        then
                for((i=0; i<${#password}; i++ ))
                do
                        ord "${password:$i:1}"

                        if [ $cap -eq 1 ] && [ $nocap -eq 1 ] && [ $num -eq 1 ]
                        then
                                brek=1
                                break
                        fi

                        if [ $ascii -ge 97 ] && [ $ascii -le 122 ]
                        then
                                nocap=1
                        elif [ $ascii -ge 65 ] && [ $ascii -le 90 ]
                        then
                                cap=1
                        elif [ $ascii -ge 48 ] && [ $ascii -le 57 ]
                                then
                                num=1
                        fi
                done
        fi

        if [ $brek == 1 ] && [ $cap -eq 1 ] && [ $nocap -eq 1 ] && [ $num -eq 1 ]
        then
                break
        else
                echo "$(date +'%D') $(date +'%T') REGISTER: ERROR password requirement do not meet" >> ./users/log.txt
                echo "REGISTER: ERROR password requirement do not meet"
        fi
done

echo $username:$password >> ./users/user.txt

echo "$(date +'%D') $(date +'%T') REGISTER: $username registered succesfully" >> ./users/log.txt
echo "register berhasil, silahkan lanjut login dengan mengakses main.sh"