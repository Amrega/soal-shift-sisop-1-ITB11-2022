#!/bin/bash

path="/home/$(whoami)"
mainFolder=$path/Documents
logFolder=$mainFolder/forensic_log_website_daffainfo_log
log=$mainFolder/log_website_daffainfo.log

#Soal A
if [ ! -d "$logFolder" ] 
then
    mkdir $logFolder
fi

#Soal B
cat $log | awk -F: '{gsub(/"/, "", $3)
 hour[$3]++; count++} END{ printf "Rata-rata serangan adalah sebanyak " count/length(hour)  "  requests per jam \n" }' >> $logFolder/ratarata.txt

#Soal C
cat $log | awk -F: '{gsub(/"/, "", $1)
 ipatac[$1]++}END{for(ip in ipatac ) { if(terbanyak < ipatac[ip] ){ terbanyak=ipatac[ip]; ips=ip  }  } printf "IP yang paling banyak mengakses server adalah: " ips " sebanyak " terbanyak " requests \n" }' >> $logFolder/result.txt

#Soal D
cat $log | awk '/curl/ {++jumlah_req_curl} END {printf "ada " jumlah_req_curl " requests yang menggunakan curl sebagai user-agent\n"}' >> $logFolder/result.txt

#Soal E
cat $log | awk -F: '{gsub(/"/, "", $3) gsub(/"/, "", $1)
 if($3 == "02"){ if(arr[$1] == 0){arr[$1]++; printf $1 " jam 2 pagi \n" }} }' >> $logFolder/result.txt
