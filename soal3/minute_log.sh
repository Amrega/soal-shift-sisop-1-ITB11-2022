#!/bin/bash

path="/home/$(whoami)"
mainFolder=$path/log

if [ ! -d "$mainFolder" ] 
then
    mkdir $mainFolder
fi

#Soal A

for ((i=0;i<=300;i++));
do
file_temp=$mainFolder/metrics.log

printf "" >> $file_temp

current_time=$(date "+%Y%m%d%H%M%S")

new_fileName=metrics_$current_time.log

printf "\nWaktu saat ini : $current_time"
 
printf "\nNama File Log: $new_fileName"
printf "\nGunakan Ctrl+C untuk menghentikan log"
memory=$(free -m | awk '/Mem/ {printf $2","$3","$4","$5","$6","$7 } ')
swap=$(free -m | awk '/Swap/ {printf $2","$3","$4 } ')
disk=$(du -sh $path | awk '{printf $1}')

printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n$memory,$swap,$path/,$disk\n" >> $file_temp

mv $file_temp $new_fileName

mv $new_fileName $mainFolder

#Soal D
chmod 600 $mainFolder/$new_fileName

#Soal B (Setelah revisi: Menggunakan cronjob)
#sleep 60

done


