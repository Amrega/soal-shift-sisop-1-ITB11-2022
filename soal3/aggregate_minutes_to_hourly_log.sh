#!/bin/bash

path="/home/$(whoami)"
mainFolder=$path/log

current_time=$(date "+%Y%m%d%H")

fileLists=$(ls $mainFolder/metrics_$current_time*)

new_fileName=metrics_agg_$current_time.log

file_temp=temp_combined.txt

printf "" >> $file_temp

printf "Nama File Agregasi Log: $new_fileName\n"

for file in $fileLists 
do
   cat $file | grep -v path >> $file_temp
done

printf "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n" > $mainFolder/$new_fileName

awk -F, '{ for(i=1;i<12;i++){ if(i!=10){ arr[i]+=$i; if(min[i] > $i || min[i] == 0){min[i]=$i }; if(max[i] < $i || max[i] == 0){ max[i]=$i }  }}; count++}END{printf "maksimum" ;for(i=1;i<12;i++){ if(i!=10){ printf ","max[i]}else{printf ","$10 }  }; printf "\nminimum" ;for(i=1;i<12;i++){ if(i!=10){ printf ","min[i]}else{printf ","$10 } }; printf "\naverage" ;for(i=1;i<12;i++){ if(i!=10){ printf ","arr[i]/count}else{printf ","$10 } }  }' $file_temp >> $mainFolder/$new_fileName

