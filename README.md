# soal-shift-sisop-1-ITB11-2022

Penjelasan dan penyelesaian Soal Shift 1 Sistem Operasi 2021 Kelompok ITB11

1. Damarhafni Rahmannabel Nadim P (5027201026)
2. Achmad Aushaf Amrega Hisyam (5027201036)

# Daftar Isi

* [Daftar Isi](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#daftar-isi)
* [Soal 1](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#soal-1)
    - [Soal 1.a.](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#soal-1a)
    - [Soal 1.b.](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#soal-1b)
    - [Soal 1.c.](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#soal-1c)
    - [Soal 1.d.](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#soal-1d)
    - [Dokumentasi Soal 1](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#dokumentasi-soal-1)
* [Soal 2](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#soal-2)
    - [Soal 2.a.](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#soal-2a)
    - [Soal 2.b.](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#soal-2b)
    - [Soal 2.c.](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#soal-2c)
    - [Soal 2.d.](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#soal-2d)
    - [Soal 2.e.](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#soal-2e)
    - [Dokumentasi Soal 2](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#dokumentasi-soal-2)
* [Soal 3](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#soal-3)
    - [Soal 3.a.](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#soal-3a)
    - [Soal 3.b.](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#soal-3b)
    - [Soal 3.c.](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#soal-3c)
    - [Soal 3.d.](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#soal-3d)
    - [Dokumentasi Soal 3](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main#dokumentasi-soal-3)
     

# Soal 1

Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program. <br>
* a. Han membuat sistem register pada script register.sh dan setiap user yang berhasil didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login yang dibuat di script main.sh <br>
* b. Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut <br>
	- i. 	Minimal 8 karakter <br>
	- ii. 	Memiliki minimal 1 huruf kapital dan 1 huruf kecil <br>
	- iii. 	Alphanumeric <br>
	- iv. 	Tidak boleh sama dengan username <br>
* c. Setiap percobaan login dan register akan tercatat pada log.txt dengan format : MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user. <br>
	- i.	Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists <br>
	- ii.	Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully <br>
	- iii.	Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME <br>
	- iv.	Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in <br>
* d. Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut : <br>
	- i.	dl N ( N = Jumlah gambar yang akan didownload) <br>Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ).  Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user. <br>
	- ii.	att <br>Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.

### Source Code

- register.sh
```shell
#!/bin/bash

ord() {
        printf -v ascii "%d" "'$1"
}

echo "welcome to soal 1"
echo "please register yourself"

if [ ! -d ./users ]
then
    mkdir users
    echo "user" >> users/user.txt
    echo "log"  >> users/log.txt
fi

cap=0
num=0
nocap=0
brek=0
same=1

file=$(awk '{FS=":"}{print $1 }' ./users/user.txt)

filearr=(`echo $file | tr ' ' ' '`)

while [ $same == 1 ]
do
        same=0
        echo "enter your username?"
        read username

        for((i=0 ;i<${#filearr} ; i++)){
                if [[ $username == ${filearr[i]} ]]
                then
                        same=1
                        echo "$(date +'%D') $(date +'%T') REGISTER: ERROR user already exist" >> ./users/log.txt
                        echo "REGISTER: ERROR user $username already exist"
                        break
                fi
        }

done

while true
do
        echo "password must contain at least 8 letter and 1 lowercase, 1 uppercase, 1 alphanumeric and cant be same as username"
        echo "enter your password?"
        read -s password

        if [ $username != $password ] && [ ${#password} -ge 8 ]
        then
                for((i=0; i<${#password}; i++ ))
                do
                        ord "${password:$i:1}"

                        if [ $cap -eq 1 ] && [ $nocap -eq 1 ] && [ $num -eq 1 ]
                        then
                                brek=1
                                break
                        fi

                        if [ $ascii -ge 97 ] && [ $ascii -le 122 ]
                        then
                                nocap=1
                        elif [ $ascii -ge 65 ] && [ $ascii -le 90 ]
                        then
                                cap=1
                        elif [ $ascii -ge 48 ] && [ $ascii -le 57 ]
                                then
                                num=1
                        fi
                done
        fi

        if [ $brek == 1 ] && [ $cap -eq 1 ] && [ $nocap -eq 1 ] && [ $num -eq 1 ]
        then
                break
        else
                echo "$(date +'%D') $(date +'%T') REGISTER: ERROR password requirement do not meet" >> ./users/log.txt
                echo "REGISTER: ERROR password requirement do not meet"
        fi
done

echo $username:$password >> ./users/user.txt

echo "$(date +'%D') $(date +'%T') REGISTER: $username registered succesfully" >> ./users/log.txt
echo "register berhasil, silahkan lanjut login dengan mengakses main.sh"
```

- main.sh
```shell
#!/bin/bash

file=$(awk '{print $1 }' ./users/user.txt)

filearr=(`echo $file | tr ' ' ' '`)

echo "welcome to soal 1"
echo "this is login"

brek=0

while true
do
    echo "enter your username?"
        read username
    echo "enter your password?"
        read -s password

    for ((i=0; i<${#filearr}; i++)){
        if [[ $username == ${filearr[i]%:*} ]] && [[ $password == ${filearr[i]#*:} ]]
        then
            brek=1
            break
        fi
    }
    
    if [[ $brek == 1 ]]
    then
        echo "$(date +'%D') $(date +'%T') LOGIN: INFO $username logged in" >> ./users/log.txt
        echo "Login berhasil"
        break
    else
        echo "$(date +'%D') $(date +'%T') LOGIN: ERROR Failed login attempt on user $username " >> ./users/log.txt
        echo "Login gagal"
    fi
done

printf "list of command : \n1. dl N -> mendownload gambar sebanyak N \n2. att -> Melihatkan data login users \n3. Exit -> logout \n"

while true
do
    read order count
    if [[ $order == 'dl' ]]
    then   
        if [[ $count == 0 ]]
        then
            echo "nothing happened because the input is 0"
        else
            nama="$(date +'%D')_$username"

            name=`echo $nama | sed 's/\//-/'` 
            name=`echo $name | sed 's/\//-/'` 
            
            if [[ ! -d ./users/$name ]]
            then
                    mkdir ./users/$name/
                    mkdir ./users/$name/$name

                    for ((i=0; i<$count; i++)){
                        fiol="PIC_$(($i+1))"
                        wget https://loremflickr.com/320/240 -O ./users/$name/$name/$fiol
                    }
                    

                    zipped="$name.zip"
                    zip -r ./users/$name/$zipped ./users/$name/$name -P $password
                    rm -r ./users/$name/$name
            else
                    zipped="$name.zip"
                    awal=$(zipinfo -t ./users/$name/$zipped | awk '{print $1}')
                    count=$(($count+$awal))

                    mkdir ./users/$name/$name

                    for ((i=$awal; i<$count; i++)){
                        fiol="PIC_$(($i+1))"
                        wget https://loremflickr.com/320/240 -O ./users/$name/$name/$fiol
                    }
                     
                    zip -r ./users/$name/$zipped ./users/$name/$name -P $password
                    rm -r ./users/$name/$name
            fi
        fi
    elif [[ $order == "att" ]]
    then
        nama=$(sed -n '/LOGIN/p' ./users/log.txt)

        while IFS= read -r line; do 
            args+=("$line") 
        done <<< $nama

        jumlog=0

        for ((i=0; i<${#args[@]}; i++)){
            if [[ ${args[i]} != ${args[i]% $username *} ]]
            then
                jumlog=$(($jumlog+1))
            fi
        }

        echo "jumlah log Login $username : $jumlog"
    elif [[ $order == "exit" ]]
    then
        echo 'logout berhasil'
        break
    else
        echo "please input the right command"
    fi
done
```

Source Code soal1 tersedia pada folder [soal1](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main/soal1)

## Soal 1.a.
### Analisa Soal

Pada soal 1.a ini, kami diminta untuk membuat sistem register dan login, dengan beberapa syarat tertentu. Setiap akun yang berhasil ter-register akan disimpan pada ./users/user.txt dan log nya akan di simpan pada ./users/log.txt.

### Cara Pengerjaan

Pertama kita buat [register.sh](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/blob/main/soal1/register.sh) yang digunakan untuk sistem register user dan berisi loop yang digunakan untuk mengecek username dan password yang lalu akan disimpan di ./users/user.txt. Kedua kita buat [main.sh](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/blob/main/soal1/main.sh) sebagai sistem login yang berisi loop juga yang digunakan untuk mengecek apakah input username dan password sesuai dengan yang ada di ./users/user.txt

### Kendala

Kendala yang dialami kelompok kami dalam mengerjakan soal 1a adalah penempatan directory ./users/user.txt dan ./users/log.txt. Serta penggunaan awk untuk mengambil data dari ./users/user.txt untuk [main.sh](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/blob/main/soal1/main.sh) yang dikarenakan masih awam cara penulisan menggunakan awk

## Soal 1.b.

### Analisa Soal

Soal 1b ini adalah bagian dari [register.sh](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/blob/main/soal1/register.sh) yang berisi syarat password yang dapat diterima oleh sistem. berikut merupakan syarat - syaratnya :

1. Minimal 8 karakter
2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
3. Alphanumeric
4. Tidak boleh sama dengan username

### Cara Pengerjaan

Dalam script [register.sh](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/blob/main/soal1/register.sh) kita menggunakan loop untuk mengecek setiap karakter pada input password, untuk minimal 8 karakter kita dapat menggunakan ${#var[@]}, untuk mengecek uppercase, Alphanumeric dan lowercase kita dapat menggunakan function bantuan yaitu ord() yang digunakan untuk mengubah karakter menjadi nilai asciinya, untuk mengecek kesamaan dengan username, kita tinggal menggunakan $username ≠ $password

## Soal 1.c.

### Analisa Soal

Pada soal 1c ini, adalah bagian soal yang membahas tentang log dari login dan register ketika berhasil dan gagal. log yang dihasilkan akan disimpan pada ./users/log.txt dengan format MM/DD/YY hh:mm:ss **MESSAGE.** message/pesan pada log akan bervariasi tergantung pada aksi yang dilakukan user. berikut merupakan variasi dan kondisinya :

1. Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
2. Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User **USERNAME** registered successfully
3. Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user **USERNAME**
4. Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User **USERNAME** logged in

### Cara Pengerjaan

Pertama, kita dapat menyiapkan format log pada kedua [register.sh](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/blob/main/soal1/register.sh) dan [main.sh](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/blob/main/soal1/main.sh). untuk mendapatkan “MM/DD/YY hh:mm:ss” dapat menggunakan “`$(date +'%D') $(date +'%T')`" lalu untuk message/pesan nya, bergantung pada kondisi yang sudah dijabarkan pada soal. Setelah format pesan sudah terbentuk, kita tinggal echo $pesan >> ./users/log.txt

## Soal 1.d.

### Analisa Soal

soal 1d ini adalah bagian dari [main.sh](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/blob/main/soal1/main.sh). Setelah user berhasil login, maka user akan berada dalam keadaan infinite while loop, dalam loop tersebut, user dapat memasukkan beberapa command, yaitu : 

1. dl N ( N = Jumlah gambar yang akan didownload)
    
    Untuk mendownload gambar dari [https://loremflickr.com/320/240](https://loremflickr.com/320/240) dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama **YYYY-MM-DD**_**USERNAME**. Gambar-gambar yang didownload juga memiliki format nama **PIC_XX**, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ).  Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.
    
2. att
    
    Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.
    
3. exit
    
    digunakan untuk logout (keluar dari infinite while loop)
    

### Cara Pengerjaan

dalam infinite while loop, terdapat beberapa kondisi, jika input berupa “att” maka script akan menghitung log Login user berhasil maupun gagal dan menampilkannya, untuk menghitung berapa jumlah lognya, kita bisa menggunakan bantuan awk lalu menggunakan ${#var[@]}. jika “exit” maka kita tinggal break dari infinite while loop. jika “dl N” maka script akan membuat directory baru dengan format nama **YYYY-MM-DD**
_**USERNAME,** lalu akan mendownload menggunakan wget pada [https://loremflickr.com/320/240](https://loremflickr.com/320/240) sebanyak N kali. setelah itu script akan mengzip semua file tersebut ke dalam zip dengan nama seperti foldernya dan dengan password yang sama dengan password user. tetapi jika directory sudah ada, maka program akan men-unzip file lalu mendownload file sebanyak N kali, lalu di-zip lagi.

### Kendala

kendala yang didapat tim kami terhadap pengerjaan soal 1d adalah pada pembuatan command “dl N” yang melibatkan algoritma yang panjang dan agak komplikasi.

## Dokumentasi Soal 1

- Register Berhasil <br>
![register berhasil](images/soal1/regisberhasil.PNG)

- Register Gagal <br>
![Register Gagal](images/soal1/regisgagal.PNG)

- Login Berhasil <br>
![Login Berhasil](images/soal1/loginberhasil.PNG)

- Login Gagal <br>
![Login Gagal](images/soal1/logingagal.PNG)

- dl <br>
![Perintah dl N](images/soal1/dl.PNG)

- Hasil Log.txt dan user.txt <br>
![Hasil Log.txt dan User.txt](images/soal1/logdanuser.PNG)

# Soal 2

Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:
- a. Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.
- b. Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.
- c. Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.
- d. Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl?
Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.
- e. Pada jam 2 pagi pada tanggal 22 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.

### Source Code

- soal2_forensic_dapos.sh
```shell
#!/bin/bash

path="/home/$(whoami)"
mainFolder=$path/Documents
logFolder=$mainFolder/forensic_log_website_daffainfo_log
log=$mainFolder/log_website_daffainfo.log

#Soal A
if [ ! -d "$logFolder" ] 
then
    mkdir $logFolder
fi

#Soal B
cat $log | awk -F: '{gsub(/"/, "", $3)
 hour[$3]++; count++} END{ printf "Rata-rata serangan adalah sebanyak " count/length(hour)  "  requests per jam \n" }' >> $logFolder/ratarata.txt

#Soal C
cat $log | awk -F: '{gsub(/"/, "", $1)
 ipatac[$1]++}END{for(ip in ipatac ) { if(terbanyak < ipatac[ip] ){ terbanyak=ipatac[ip]; ips=ip  }  } printf "IP yang paling banyak mengakses server adalah: " ips " sebanyak " terbanyak " requests \n" }' >> $logFolder/result.txt

#Soal D
cat $log | awk '/curl/ {++jumlah_req_curl} END {printf "ada " jumlah_req_curl " requests yang menggunakan curl sebagai user-agent\n"}' >> $logFolder/result.txt

#Soal E
cat $log | awk -F: '{gsub(/"/, "", $3) gsub(/"/, "", $1)
 if($3 == "02"){ if(arr[$1] == 0){arr[$1]++; printf $1 " jam 2 pagi \n" }} }' >> $logFolder/result.txt
```

Source Code soal2 tersedia pada folder [soal2](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main/soal2)

## Soal 2.a.

### Analisa Soal

Pada soal 2a ini, kita tinggal membuat folder dengan nama “forensic_log_website_daffainfo_log”

### Cara Pengerjaan

Kita dapat membuat directory folder baru dengan menggunakan bantuan command mkdir

## Soal 2.b.

### Analisa Soal

Pada soal 2b ini, kita diminta menghitung rata - rata serangan pada website [https://daffa.info](https://daffa.info/) per jamnya. Untuk mendapat lognya kita bisa melihat di log yang sudah disediakan.

### Cara Pengerjaan

Untuk mengerjakan soal 2b ini, kita dapat menggunakan awk dengan separator ( : ) saja untuk menghitung rata - rata request per jam nya. juga ditambah dengan gsub agar tanda petik (”) dihilangkan. kita dapat mengambil $3 untuk menghitung jam nya dan akan dikelompokkan pada array, kita juga akan menghitung semua jumlah serangan menggunakan $count++. lalu untuk hasilnya kita bisa menggunakan rumus $count/${#array[@]} >> ratarata.txt

### Kendala

Kendala yang didapat tim kami terhadap pengerjaan soal 2b adalah penggunaan awk yang agak membingungkan dan belum terbiasa.

## Soal 2.c.

### Analisa Soal

Pada soal 2c ini, kita diminta menghitung IP dengan jumlah request terbanyak.

### Cara Pengerjaan

Untuk mengerjakan soal 2c ini, kita dapat menggunakan awk dengan separator ( : ) saja untuk mencari dan menghitung IP dengan request terbanyak. juga ditambah dengan gsub agar tanda petik (”) dihilangkan. kita dapat mengambil $1 untuk mendapatkan IP, lalu kita simpan ke dalam array jumlah request setiap IP dengan index menunjukkan IP dan element menunjukkan jumlah request yang dilakukan IP tersebut. setelah itu kita dapat menggunakan loop dan algoritma mencari modus dalam soal ini, setelah itu, hasilnya disimpan dalam result.txt 

### Kendala

Kendala yang didapat tim kami terhadap pengerjaan soal 2c adalah penggunaan awk yang agak membingungkan dan belum terbiasa.

## Soal 2.d.

### Analisa Soal 2.d.

Pada soal 2d ini, kita diminta menghitung jumlah user-agent yang menggunakan curl dalam melakukan request.

### Cara Pengerjaan

Untuk mengerjakan soal 2d ini, kita dapat menggunakan awk dengan separator ( /curl/ ) saja untuk mencari dan menghitung IP dengan request terbanyak. setelah itu kita simpan jumlahnya pada var ++$jumlah_req_curl. Lalu hasilnya disimpan pada result.txt

### Kendala

Kendala yang didapat tim kami terhadap pengerjaan soal 2d adalah penggunaan awk yang agak membingungkan dan belum terbiasa.

## Soal 2.e.

## Analisa Soal 2.e.

Pada soal 2e ini, kita diminta mendapatkan IP apa saja yang menyerang pada jam 2 pagi tersebut.

### Cara Pengerjaan

Untuk mengerjakan soal 2c ini, kita dapat menggunakan awk dengan separator ( : ) saja untuk mencari dan menghitung IP dengan request terbanyak. juga ditambah dengan gsub agar tanda petik (”) dihilangkan. kita dapat mengambil $1 untuk mendapatkan IP dan $3 untuk mendapatkan jam. Dalam soal 2e ini, kita bisa menggunakan bantuan if dalam mengambil  data,  dengan  kondisi [ $3 == 02 ] .jika kondisi tersebut bernilai TRUE, maka print $1. hasilnya masukkan ke dalam result.txt

### Kendala

kendala yang didapat tim kami terhadap pengerjaan soal 2e adalah penggunaan awk yang agak membingungkan dan belum terbiasa.

## Dokumentasi Soal 2

- ratarata.txt <br>
![ratarata.txt](images/soal2/ratarata.PNG)

- result.txt <br>
![result.txt](images/soal2/result.PNG)

# Soal 3

Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer.

Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

- a. Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.
- b. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.
- c. Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log
- d. Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

### Source Code

- aggregate_minutes_to_hourly_log.sh
```shell
#!/bin/bash

path="/home/$(whoami)"
mainFolder=$path/log

current_time=$(date "+%Y%m%d%H")

fileLists=$(ls $mainFolder/metrics_$current_time*)

new_fileName=metrics_agg_$current_time.log

file_temp=temp_combined.txt

printf "" >> $file_temp

printf "Nama File Agregasi Log: $new_fileName\n"

for file in $fileLists 
do
   cat $file | grep -v path >> $file_temp
done

printf "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n" > $mainFolder/$new_fileName

awk -F, '{ for(i=1;i<12;i++){ if(i!=10){ arr[i]+=$i; if(min[i] > $i || min[i] == 0){min[i]=$i }; if(max[i] < $i || max[i] == 0){ max[i]=$i }  }}; count++}END{printf "maksimum" ;for(i=1;i<12;i++){ if(i!=10){ printf ","max[i]}else{printf ","$10 }  }; printf "\nminimum" ;for(i=1;i<12;i++){ if(i!=10){ printf ","min[i]}else{printf ","$10 } }; printf "\naverage" ;for(i=1;i<12;i++){ if(i!=10){ printf ","arr[i]/count}else{printf ","$10 } }  }' $file_temp >> $mainFolder/$new_fileName
```

- minute_log.sh
```shell
#!/bin/bash

path="/home/$(whoami)"
mainFolder=$path/log

if [ ! -d "$mainFolder" ] 
then
    mkdir $mainFolder
fi

#Soal A

for ((i=0;i<=300;i++));
do
file_temp=$mainFolder/metrics.log

printf "" >> $file_temp

current_time=$(date "+%Y%m%d%H%M%S")

new_fileName=metrics_$current_time.log

printf "\nWaktu saat ini : $current_time"
 
printf "\nNama File Log: $new_fileName"
printf "\nGunakan Ctrl+C untuk menghentikan log"
memory=$(free -m | awk '/Mem/ {printf $2","$3","$4","$5","$6","$7 } ')
swap=$(free -m | awk '/Swap/ {printf $2","$3","$4 } ')
disk=$(du -sh $path | awk '{printf $1}')

printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n$memory,$swap,$path/,$disk\n" >> $file_temp

mv $file_temp $new_fileName

mv $new_fileName $mainFolder

#Soal D
chmod 600 $mainFolder/$new_fileName

#Soal B
#sleep 60

done
```


Source Code soal3 tersedia pada folder [soal3](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/tree/main/soal3)

## Soal 3.a.

### Analisa Soal

Pada soal 3.a., kami diminta untuk membuat file log yang berisi hasil monitoring ram dan size dari suatu directory yakni /home/user dengan file script bash dan file log yang dibuat memiliki nama dengan format metrics_YmdHms.log dengan YmdHms merupakan waktu saat program dijalankan.

### Cara Pengerjaan

Ketika Program [minute_log.sh](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/blob/main/soal3/minute_log.sh) dijalankan, hal pertama yang dilakukan yakni membuat folder log untuk menyimpan seluruh log yang akan dibuat oleh program. Disini kami menggunakan kondisi if-then untuk membuat folder tersebut. Apabila folder tidak ada maka program akan membuat folder tersebut.

Setelah itu, program akan menggunakan for loop untuk membuat file temporary atau file sementara dengan nama metrics.log dan melakukan deklarasi waktu saat ini dengan variabel current_time dan deklarasi nama file akhir yakni metrics_$current_time.log.

Kemudian program akan mencetak waktu saat ini dan nama file log yang akan memiliki data log.

Selanjutnya program akan menggunakan fungsi mv untuk mengubah nama file sementara yakni metrics.log menjadi nama file baru yakni metrics_$current_time.log dan memindahkan log ke folder log yang telah dibuat.

Setelah itu, program akan mendapatkan data ram dan disk dari /home/user, dan mencetaknya. Setelah dicetak maka data yang ada akan dimasukkan ke dalam file log yang telah dibuat.

### Kendala

Kendala yang dialami kelompok kami yakni pada hasil monitoring ram dan disk. Awalnya kami melakukan command free -m yang dimana menghasilkan output berbentuk tabel, sedangkan untuk output yang diminta yakni berupa 2 baris dengan baris pertama merupakan label data dan baris kedua merupakan data itu sendiri.

## Soal 3.b.

### Analisa Soal

Pada soal 3.b., kami diminta untuk memodifikasi file log [minute_log.sh](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/blob/main/soal3/minute_log.sh) yang dibuat berdasarkan soal 3.a. agar dapat berjalan otomatis setiap menit.

### Cara Pengerjaan

Sebelum revisi, kami menggunakan kode berdasarkan soal 3.a. yakni [minute_log.sh](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/blob/main/soal3/minute_log.sh) dan menambahkan command sleep dalam for-loop agar log dapat tercatat otomatis setiap 60 detik. Disini command sleep menggunakan satuan sekon atau detik sehingga dapat menggunakan sleep 60. Namun, setelah di revisi, kami menggunakan cron jobs untuk otomasi setiap 1 menitnya. Kami mengatur waktu `cron` dengan `* * * * *` dimana setiap menitnya akan melakukan bash di path script file yakni `/home/{user}/minute_log.sh` dan `/home/{user}/aggregate_minutes_to_hourly_log.sh`.

### Kendala

Kendala yang dialami kelompok kami yakni pada penggunaan cronjob agar script dapat berjalan otomatis pada setiap menit, alhasil kami menggunakan command sleep yang menggunakan satuan detik sehingga kami menggunakan sleep 60. Namun, setelah di revisi, kami berhasil untuk menggunakan cron jobs untuk otomasi setiap 1 menitnya.

## Soal 3.c.

### Analisa Soal 3.c.

Pada soal 3.c., kami diminta untuk membuat script [aggregate_minutes_to_hourly_log.sh](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/blob/main/soal3/aggregate_minutes_to_hourly_log.sh) untuk membuat agregasi file log ke satuan jam. File akan berisi info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics.

### Cara Pengerjaan

Ketika Program [aggregate_minutes_to_hourly_log.sh](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/blob/main/soal3/aggregate_minutes_to_hourly_log.sh) dijalankan, hal pertama yang dilakukan yakni membuat file agregasi log. Disini kami membuat file temporary agar dapat mengumpulkan seluruh metrics dalam satu jam.

Setelah itu, program akan menggunakan for loop untuk membuat file temporary atau file sementara dengan nama `temp_combined.txt` agar dapat mengumpulkan seluruh metrics dalam satu jam dan melakukan deklarasi waktu saat ini dengan variabel current_time dan deklarasi nama file akhir yakni `metrics_agg_$current_time.log`.

Kemudian program akan mengambil seluruh file yang memiliki nama `/metrics_$current_time` dan mengumpulkannya ke dalam `temp_combined.txt`dan menghilangkan line yang memiliki `path`sehingga hanya mengambil angka-angkanya saja.

Selanjutnya program akan mencetak `type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n` dan menggunakan `awk` untuk memasukkan data yang ada berupa minimum, maksimum, dan rata-rata ke dalam file agregasi.

### Kendala

Kendala yang dialami kelompok kami yakni pada hasil agregasi, kami belum dapat menemukan cara untuk menentukan data rata-rata dengan sebuah string path dikarenakan kendala waktu. Namun kami telah menemukan cara yang diminta sesuai soal pada saat revisi praktikum modul 1.

## Soal 3.d.

### Analisa Soal 3.d.

Pada soal 3.d., kami diminta untuk memodifikasi script dari soal 3.a. yakni [minute_log.sh](https://gitlab.com/Amrega/soal-shift-sisop-1-ITB11-2022/-/blob/main/soal3/minute_log.sh) untuk membuat file log hanya dapat dibaca oleh user pemilik file.

### Cara Pengerjaan

Disini kami menggunakan command `chmod` untuk mengubah hak pada file agar dapat dibaca oleh user pemilik file saja.  Maka kami menggunakan `chmod 600` untuk mengubah hak user menjadi read and write only, kemudian group memiliki hak access none, dan other memiliki hak akses none.

## Dokumentasi Soal 3

- Terminal Bash minute_log.sh <br>
![Terminal bash minute_log](images/soal3/Terminal%20bash%20minute_log.png)

- Hasil Log <br>
![Hasil Log](images/soal3/Hasil%20Log.png)

- Hasil Bash minute_log.sh <br>
![Hasil Bash minute_log](images/soal3/Hasil%20bash%20minute_log.png)

- Terminal Bash aggregate_minutes_to_hourly_log.sh <br>
![Terminal bash aggregate_minutes_to_hourly_log.sh](images/soal3/Terminal%20bash%20aggregate.png)

- Hasil Log Agregasi <br>
![Hasil Log Agregasi](images/soal3/Hasil%20Log%20Agregasi.png)

- Hasil Bash aggregate_minutes_to_hourly_log.sh <br>
![Hasil Bash aggregate_minutes_to_hourly_log.sh](images/soal3/Hasil%20bash%20aggregate.png)

- Cron untuk otomasi setiap 1 menit <br>
![Cron untuk otomasi setiap 1 menit](images/soal3/cron.png)
